![banner](https://gitlab.com/celn-pro/celn-pro/-/raw/cc47a1b5214e541e0666c5ca4840f045de2856b5/logo1.png)

# Hi there! 👋 I'm Ayubu Joseph
## Full Stack Developer | Blockchain Enthusiast

I'm a full stack developer experienced in crafting modern web and blockchain applications. Currently, I'm delving into the world of Web3!

![nft marketplace](https://gitlab.com/celn-pro/celn-pro/-/raw/552e2d4adcfd70a941be520c9f8da86dfdb5fa70/images.jpeg){width=45%}

Below are some key projects I've been working on:

- [AI customizable shirt](https://gitlab.com/celn-pro/three_pro) - customizable shirt using threejs, framer motion, reactjs, mongoDB, tailwind, and the openAI API.
- [AI generated gallery website](https://gitlab.com/celn-pro/dall-e) - AI-generated images using react, tailwind, framer motion, mongoose, nodejs, and the openAI API.
![defi app](https://gitlab.com/celn-pro/celn-pro/-/raw/1cdd551c8b3419ed4c298d21a7120d1a5b3d5ad3/dall_e.jpeg){width=45%}

- [NFT Marketplace](https://gitlab.com/celn-pro/hardhat-nft-fcc) - An Ethereum blockchain NFT marketplace built with Solidity, Hardhat, React, and TailwindCSS.
- [DeFi Staking App](https://gitlab.com/celn-pro/defi-staking-app) - A decentralized finance app with staking rewards, built using Ethereum, Solidity, TypeScript, and React.
- [Crypto Trading Bot](https://gitlab.com/celn-pro/crypto-trading-bot) - Automated cryptocurrency trading bot using Python, NumPy, Pandas, and the Binance API.

🌱 Currently learning Rust and exploring deeper into Polkadot and Solana ecosystems.

👯 I’m open to collaborations on innovative Web3, Metaverse, or Blockchain projects.

💬 Ask me about JavaScript, React, Solidity, Node.js, or Web Development.

⚡ Fun fact: When I'm not coding, you'll find me scuba diving or doing astrophotography.

### Tech Stack

**Blockchain**
![Solidity](https://simpleicons.org/icons/solidity.svg){width=40}
![Rust](https://simpleicons.org/icons/rust.svg){width=40}

**Frontend**
![JavaScript](https://simpleicons.org/icons/javascript.svg){width=40}
![React](https://simpleicons.org/icons/react.svg){width=40}
![Redux](https://simpleicons.org/icons/redux.svg){width=40}
![TailwindCSS](https://simpleicons.org/icons/tailwindcss.svg){width=40}

**Backend**
![Node.js](https://cdn.jsdelivr.net/npm/simple-icons@v10/icons/nodedotjs.svg){width=50}



![ExpressJS](https://simpleicons.org/icons/express.svg){width=40}
![MongoDB](https://simpleicons.org/icons/mongodb.svg){width=50}

**Tools**
![Git](https://simpleicons.org/icons/git.svg ){width=40}
![GitHub](https://simpleicons.org/icons/github.svg){width=40}
![GitLab](https://simpleicons.org/icons/gitlab.svg){width=40}
<!--![Bitbucket](https://gitlab.com/celn-pro/celn-pro/-/raw/master/bitbucket-original-wordmark.svg){width=40} -->

### Let's Connect

[![Twitter](https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/CelnPro)
<!-- [![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/yourprofile) -->












































